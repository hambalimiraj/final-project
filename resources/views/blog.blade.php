@extends('layouts.app')
@section('content')
@foreach ($postingan as $item)

<div class="container">
<div class="card mt-3 mb-0" style="width: 50rem;">
    <div class="row">
      <div class="col-md-8">
        <div class="card-body">
          <a href="blog/{{$item->id}}" style="text-decoration:none" class="text-dark">
            <h5 class="card-title">{{$item->judul}}</h5>   
          </a>
          <p class="card-text">{{$item->isi}}</p>
          <img src="https://res.cloudinary.com/dbpyiy8on/image/upload/{{$item->image}}" class="img-thumbnail" style="max-width: 680px">
          <p class="card-text"><small class="text-muted">{{$item->created_at}}</small></p>
          <button type="button" class="btn btn-primary btn-sm"><a href="/blog/{{$item->id}}" style="text-decoration:none" class="text-light"> Komentar </a> </button>
        </div>
      </div>
      </div>
    </div>
  </div>
      @endforeach
      @endsection
      <div class="sticky-top">
      <div class="container">
      <div class="col-sm mt-2 p-2">
            <div class="card mt-5 float-right pt-2" style="width: 15rem;">
              <img class="card-img-top align-self-center rounded-circle" src="/frontend/images/people.png" alt="Card image cap" style="max-width: 100px">
                <div class="card-body">
                  <a href="profile" style="text-decoration:none" class="text-dark">
                  <h5 class="card-title">{{ Auth::user()->name }}</h5>
                  </a>
                </div>
                <ul class="list-group list-group-flush">
                  <li class="list-group-item text-white bg-dark text-center">A  B  O  U  T</li>
                  <li class="list-group-item text-center">{{ Auth::user()->bio }}</li>
                  {{--  <li class="list-group-item">Jenis Kelamin : {{ Auth::user()->jenis_kelamin }}</li>  --}}
                  <li class="list-group-item">Status : {{ Auth::user()->status }}</li>
                  <li class="list-group-item">Agama : {{ Auth::user()->agama }}</li>
                  <li class="list-group-item">Alamat : {{ Auth::user()->alamat }} </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
