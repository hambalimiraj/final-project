@extends('layouts.app')
@section('content')
    <div class="container">
   
                    <div class="card mt-5" style="width: 40rem;">
                    <div class="row no-gutters">
                      <div class="col">
                        <div class="card-body">
                         
                          <h5 class="card-title">{{$blog->judul}}</h5>
                       
                          <p class="card-text">{{$blog->isi}}</p>
                          <img src="{{$blog->image}}" class="img-thumbnail" >
                          <p class="card-text"><small class="text-muted">{{$blog->created_at}}</small></p>
                          
                          @if($blog->user->id == Auth::user()->id)
                          <div class="d-flex">
                          <button type="button" class="btn btn-light"> <a href="/blog/{{$blog->id}}/edit" class="text-dark"> Edit </a> </button>                     
                          <form action="{{route('blog.destroy',[$blog->id])}}" method="POST">
                            {{csrf_field()}}
                            {{method_field('DELETE')}}
                          {{--  < type="submit" class="btn btn-danger btn-sm mt-2">  --}}
                            <input type="submit" value="Delete"  class="btn btn-outline-danger">
              
                          </form>
                          </div>
                          @endif
                          

                      <div class="mt-2">
                        @comments(['model' => $blog])
                      </div>



                        </div>
                      </div>
                    </div>
                  </div>
                </div>
@endsection

