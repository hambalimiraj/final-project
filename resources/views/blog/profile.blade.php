@extends('layouts.app')
@section('content')
    
{{--  Bagian foto profil  --}}
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        
        <img src="/frontend/images/people.png" width="200" class="Rounded-circle">
        <h1 class="display-4" name="name">{{ Auth::user()->name }}</h1>
        <p class="lead"> {{ Auth::user()->bio }} </p>
        
     </div>
  </div>

{{--  Menampilkan postingan pertanyaan  --}}
<div class="container">
    <div class="row row-cols-2">
      <div class="col-4">
        
        <ul class="list-group">
            <li class="list-group-item text-white bg-dark text-center">A  B  O  U  T</li>
            <li class="list-group-item">Status : {{ Auth::user()->status }}</li>
            <li class="list-group-item">Agama : {{ Auth::user()->agama }}</li>
            <li class="list-group-item">Alamat : {{ Auth::user()->alamat }} </li>
            <li class="list-group-item">
                
                  <a href="/profile/{{Auth::user()->id}}/edit" class="text-dark btn btn-outline-secondary btn-lg btn-block"> C H A N G E </a>
                
            </li>
          </ul>
      
    </div>
    <div class="col-8">
          
    @foreach ($blog as $item)
        
    <div class="card mt-1 mb-3" style="width: 40rem;">
            <div class="row">
              <div class="col-md-8">
                <div class="card-body">
        
                  <a href="blog/{{$item->id}}" style="text-decoration:none" class="text-dark">
                 
                    <h5 class="card-title">{{$item->judul}}</h5>
                  
                  </a>
        
                
                  <p class="card-text">{{$item->isi}}</p>
                  <img src="https://res.cloudinary.com/dbpyiy8on/image/upload/{{$item->image}}" class="img-thumbnail" style="max-width: 600px">
                  <p class="card-text"><small class="text-muted">{{$item->created_at}}</small></p>
                  <button type="button" class="btn btn-primary btn-sm"><a href="/blog/{{$item->id}}" style="text-decoration:none" class="text-light"> Komentar </a> </button>
                </div>
              </div>
            </div>
          </div>
    
    
    @endforeach    
    
        
        </div>
    </div>
</div>

@endsection