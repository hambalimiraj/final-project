@extends('layouts.app')
@section('content')
     <!-- Content Header (Page header) -->
 <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h4 class="m-0 text-dark">Edit Postingan</h4>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">



                        <form class="container" action="/blog/{{$blog->id}}" method="POST" enctype="multipart/form-data">
                          {{csrf_field()}}
                            {{ method_field('PUT') }}
                              <div class="form-group">
                                <label for="exampleFormControlInput1">Judul</label>
                                <input type="text" class="form-control" id="exampleFormControlInput1" name="judul" value="{{$blog->judul}}">
                              </div>
                              <div class="form-group">
                                <label for="exampleFormControlTextarea1">Isi</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Isi Postingan" name="isi">{{$blog->isi}}</textarea>
                              </div>
                              <div class="input-group">
                                <div class="custom-file">
                                  <input type="file" class="custom-file-input" id="inputGroupFile04" aria-describedby="inputGroupFileAddon04" name="gambar">
                                  <label class="custom-file-label" for="inputGroupFile04">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                  <button class="btn btn-outline-secondary" type="submit" id="inputGroupFileAddon04">Submit</button>
                                </div>
                              </div>
                          
                            </form>




                        {{--  <form action="/blog/{{$blog->id}}" method="POST">
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                              <label for="exampleInputEmail1">Judul Pertanyaan</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name="judul" value="{{$blog->judul}}">
                            </div>
                            <div class="form-group">
                              <label for="exampleInputPassword1">Isi</label>
                              <textarea class="form-control" id="exampleInputPassword1" name="isi">{{$blog->isi}} </textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Edit</button>
                          </form>  --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection