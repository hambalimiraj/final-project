@extends('layouts.app')
<div class="container">
    <div class="row">
        <div class="col-md mt-4">
<div class="card mt-5">
<div class="card-body">
    <form class="container" action="/profile/{{$profile->id}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('PUT')}}

        <div class="form-group">
            <label for="inputNama">Nama</label>
            <input type="text" class="form-control" id="inputNama" placeholder="Nama Lengkap" value="{{$profile->name}}" name="name">
        </div>
        <div class="form-group">
            <label for="inputBio"> Bio </label> 
            <input class="form-control" type="text" placeholder="Bio" value="{{$profile->bio}}" name="bio">
        </div>
        <div class="form-group">
          <label for="inputAddress">Alamat</label>
          <input type="text" class="form-control" id="inputAddress" placeholder="Alamat" value="{{$profile->alamat}}" name="alamat">
        </div>
        <div class="form-group">
          <label for="inputAgama">Agama</label>
          <input type="text" class="form-control" id="inputAgama" placeholder="Agama" value="{{$profile->agama}}" name="agama">
        </div>
        <div class="form-group">
            <label for="inputStatus">Status</label>
            <input type="text" class="form-control" id="inputStatus" placeholder="Status" value="{{$profile->status}}" name="status">
          </div>


          {{--  <select class="form-control" value={{$profile->jenis_kelamin}} name="jenis_kelamin">
            <option value="Laki-laki">Laki-laki</option>
            <option value="Perempuan">Perempuan</option>
          </select>  --}}

            <button type="submit" class="btn btn-outline-secondary btn-lg btn-block mt-4">Edit</button>
        </form>
    </div>
</div>
</div>
</div>
</div>
</div>