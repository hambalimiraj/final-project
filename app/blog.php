<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravelista\Comments\Commentable;

class blog extends Model
{
    protected $table = 'blogs';
    protected $fillable = ['judul','isi','image'];
    use Commentable;
    public function user(){
        return $this->belongsTo('App\User');
    }

}
