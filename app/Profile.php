<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'users';
    protected $fillable = ['jenis_kelamin', 'status', 'bio', 'alamat', 'agama'];
}
