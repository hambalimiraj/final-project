<?php

namespace App\Http\Controllers;

use App\blog;
use Illuminate\Http\Request;
use Auth;
use Alert;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {

        \Cloudinary::config([

            "cloud_name" => "dbpyiy8on",
            "api_key" => "971919886571495",
            "api_secret" => "MCv9__sqfovBlFEAYV6Dtdeh6oA"

        ]);

    }

    public function index()
    {
        $postingan = Blog::all();

        return view('blog')->with(['postingan' => $postingan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $blog = new Blog;
        $data = $request->all();
        if ($files = $request->file('gambar')) {
            $destinationPath = 'gambar/';
            $profileImage = date('YmdHis') ;
            //. "." . $files->getClientOriginalExtension()
            // $files->move($destinationPath, $profileImage);

            \Cloudinary\Uploader::upload($files, ["public_id" => $profileImage]);

            $blog->image = $profileImage;
        }
        $blog->judul = $request->judul;
        $blog->isi = $request->isi;
        $blog->user_id = Auth::user()->id;
        $blog->save();

        Alert::success('success', 'Post was Created!');

        return redirect(route('blog.index'));
    }

    /** 
     * Display the specified resource.
     *
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::find($id);
        $blog->image=\Cloudinary::cloudinary_url($blog->image);
        return view('blog.show')->with(['blog' => $blog]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('blog.edit')->with(['blog' => $blog]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $blog = Blog::find($id);

        // $blog = new Blog;
        $blog->judul = $request->judul;
        $blog->isi = $request->isi;
       
        $blog->user_id = Auth::user()->id;

        $data = $request->all();
        if ($request->hasfile('gambar')) {
            $files = $request->file('gambar');
            // $destinationPath = '';
            $profileImage = date('YmdHis') ;
            // . "." . $files->getClientOriginalExtension();
            // $files->move('gambar/', $profileImage);

            \Cloudinary\Uploader::upload($files, ["public_id" => $profileImage]);
            $blog->image = $profileImage;
        }
     
        $blog->save();

        return redirect('blog')->with('blog', $blog);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $item =Blog::find($id);
       $item->delete();
       return redirect('blog');
    }
}
